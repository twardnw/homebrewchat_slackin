# homebrewchat slackin

Image built from https://hub.docker.com/r/chk1/slackin/

To run this service, two environment variables must be set in OpenShift
 * `SLACK_TOKEN`
    This is the token of the user who 'invites' to the slack. A token can be retrieved from https://api.slack.com/custom-integrations/legacy-tokens
 * `SLACK_SUBDOMAIN`
    The subdomain belonging to the slack org. For dev we use `amazeeiodev`, prod is `amazeeio`

Example: `oc env dc/amazeeio-slackin-develop SLACK_SUBDOMAIN=homebrewing`

Further reading about environment variables in OpenShift can be found here : https://docs.openshift.com/enterprise/3.0/dev_guide/environment_variables.html
